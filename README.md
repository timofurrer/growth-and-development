# Growth and Development

This is a personal repository to help me track my personal growth and development. 
It's intentially a public project to foster transparancy and some form of higher accountability - at least that's what I tell myself.

In particular I use it to track:

* personal [Learning and Development](https://about.gitlab.com/handbook/people-group/learning-and-development/) plans
* noteworthy [achievements](https://about.gitlab.com/handbook/values/#results)
* weekly progress on things I work on, see ~"weekly"
* alignment with [CREDIT values](https://about.gitlab.com/handbook/values/), see ~"value"

*Disclaimer: this project will never be complete and I'll intentionally and unintentially keep things undisclosed.*

## More about me ...

... can be found in my personal [README](https://gitlab.com/timofurrer) on my profile.

## Usage

There is some automation in place to keep maintenance low-effort and ping me automatically if my attention is required.

### Weekly Issue

The weekly issue is automatically created every Monday at 7am Zurich timezone using the [*Create Weekly* scheduled pipeline](https://gitlab.com/timofurrer/growth-and-development/-/pipeline_schedules).
This will also asign me, create a To-Do and correctly set the due date leveraging the [weekly template](.gitlab/issue_templates/weekly.md).
